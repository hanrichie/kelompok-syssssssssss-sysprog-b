#!/bin/bash

# Purpose: Change timezone to Asia timezone
export TZ=Asia/Jakarta

# Purpose: Display pause prompt
# $1-> Message (optional)
function pause(){
	local message="$@"
	[ -z $message ] && message="Press [Enter] key to continue..."
	read -p "$message" readEnterKey
}

# Purpose - Display header message
# $1 - message
function write_header(){
	local h="$@"
	echo "---------------------------------------------------------------"
	echo "                         ${h}"
	echo "---------------------------------------------------------------"
}

# Purpose - Mute/unmute the audio 
function mute(){
    echo "Muting the audio."
	amixer -q sset Master mute
    echo "Done."
	#pause "Press [Enter] key to continue..."
	pause
}

# Purpose - increace audio volume
function volume_up(){
    # echo "Increase audio volume by:   (%)"
	read -p "Increase audio volume by: (%)" NUM
	amixer -q sset Master "$NUM"%+
    echo "Audio volume increase by $NUM%."
	#pause "Press [Enter] key to continue..."
    echo "Done."
	pause
}

# Purpose - decrease audio volume
function volume_down(){
    # echo "decrease audio volume."
	read -p "Decrease audio volume by: (%)" NUM
	amixer -q sset Master "$NUM"%-
	echo "Audio volume decrease by $NUM%."
	#pause "Press [Enter] key to continue..."
    echo "Done."
	pause
}

# Purpose - Do toggle setting
function toggle(){
    echo "Toogle audio."
	amixer -q sset Master toggle
	#pause "Press [Enter] key to continue..."
	echo "Done"
	pause
}

# Purpose - Display morse generator
function morse(){
	write_header " MORSE Generator " # call write_header
	echo "Press any vowel only."
	echo "Press 1 to exit"
	while true;
	do
		stty -echo raw
		c=$(dd bs=1 count=1 2>/dev/null )
		stty echo -raw
		echo "You press: $c"
		case $c in
			a) aplay -q ./morse_sound/a_morse.wav ;;
			i) aplay -q ./morse_sound/i_morse.wav ;;
			u) aplay -q ./morse_sound/u_morse.wav ;;
			e) aplay -q ./morse_sound/e_morse.wav ;;
			o) aplay -q ./morse_sound/o_morse.wav ;;
			1) echo "Back to main page"; main ;;
			*) echo "Please choose a i u e o only"
				pause
		esac
	done
	#pause "Press [Enter] key to continue...
	pause
}

# Purpose - Display a menu on screen
function show_menu(){
    # date
    echo "==========================="
    echo "         MAIN MENU"
    echo "==========================="
    echo "1. Audio Setting"
    echo "2. Morse Translator"
    echo "3. Exit"
}

# Purpose - Display a audio menu on screen
function show_menu_audio(){
    echo "============================="
	echo "        AUDIO SETTING"
	echo "============================="
    echo "1. Volume UP"
    echo "2. Volume DOWN"
	echo "3. Mute"
    echo "4. Toggle"
    echo "5. Back"
}

# Purpose - Get input via the keyboard and make a decision using case..esac for audio menu 
function read_input_audio(){
	local l
	read -p "choose 1 - 5: " l
	case $l in
		1)	volume_up ;;
		2)	volume_down ;;
		3)	mute ;;
		4)  toggle ;;
        5)	echo "OKE"; main ;;
		*)	
			echo "Please select between 1 to 5 choice only."
			#pause "Press [Enter] key to continue..."
			pause
	esac
}

# Purpose - Get info about your hardware detail
function audio_setting(){
	while true
	do
		clear
 		show_menu_audio	# display audio memu
        amixer get Master | grep "Front Left:" 
        amixer get Master | grep "Front Right:" 
 		read_input_audio  # wait for user input
	done
	
	#pause "Press [Enter] key to continue..."
	pause
}
# Purpose - Get input via the keyboard and make a decision using case..esac 
function read_input(){
	local c
	read -p "choose 1 - 3: " c
	case $c in
		1)	audio_setting ;;
		2)	morse ;;
		3)	echo "Bye Bye...."; exit 0 ;;
		*)	
			echo "Please select between 1 to 3 choice only."
			pause
	esac
}

# ignore CTRL+C, CTRL+Z and quit singles using the trap
trap '' SIGINT SIGQUIT SIGTSTP

# main logic
function main(){
     echo "Starting device test code..."
	while true
	do
		clear
          echo
		show_menu	# display memu
		read_input  # wait for user input
	done
}

while true
do
	main
done
