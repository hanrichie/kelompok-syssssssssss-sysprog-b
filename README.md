# Kelompok Syssssssssss - SysProg B

Proyek tugas akhir mata kuliah System Programming.

Kelompok Syssssssssss, Kelas B.

Hanrichie, M Ilham Alghifari, Stephen Handiar.


Cara menggunakan script:

1. Git clone repository

2. Ubah file .bashrc sesuai dengan file .bashrc pada repository (agar langsung berjalan ketika boot system)

3. Ubah file tersebut menggunakan sudo nano ~/.bashrc

4. Install alsa-utils dengan sudo apt-get install alsa-utils

5. Restart session PuTTY agar script berjalan ketika boot system

6. Akan muncul tampilan CLI (main menu)

7. Ketik 1 untuk konfigurasi mixer, 2 untuk mengetes morse code

8. Ketik huruf hidup (a, i, u, e, o), lalu morse code yang bersesuaian akan berbunyi

9. Tunggu sekian detik sebelum mengetik huruf selanjutnya, agar tidak error

10. Ketik 1 untuk kembali ke main menu